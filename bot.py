from pyrogram import Client, MessageHandler

forwards = [
    -1001386781644,
    -1001434860064
]


def my_function(client, message):
    if message.chat.id in forwards:
        app.forward_messages('@test0wn', message.chat.id, message.message_id)


my_handler = MessageHandler(my_function)

app = Client("my_account")

app.start()
app.add_handler(my_handler)
